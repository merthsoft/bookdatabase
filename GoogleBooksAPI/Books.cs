﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GoogleBooksAPI {
    public static class Books {
        const string ENDPOINT_URL = "https://www.googleapis.com/books/v1/volumes?q={0}";

        public static async Task<GoogleBook> GetBookDetails(string query) {
            using (HttpClient client = new HttpClient()) {
                HttpRequestMessage reqMessage = new HttpRequestMessage();
                reqMessage.RequestUri = new Uri(string.Format(ENDPOINT_URL, query));

                HttpResponseMessage respMessage = await client.SendAsync(reqMessage);
                JsonSerializer deserializer = new JsonSerializer();
                using (var stream = await respMessage.Content.ReadAsStreamAsync())
                using (var sr = new StreamReader(stream))
                using (JsonReader reader = new JsonTextReader(sr)) {
                    var rootObject = deserializer.Deserialize<RootObject>(reader);
                    var firstItem = rootObject.items[0];
                    var firstBook = firstItem.volumeInfo;

                    GoogleBook book = new GoogleBook() {
                        Title = firstBook.title,
                        ISBN10 = firstBook.industryIdentifiers.FirstOrDefault(s => s.type == "ISBN_10").identifier,
                        ISBN13 = firstBook.industryIdentifiers.FirstOrDefault(s => s.type == "ISBN_13").identifier,
                        Authors = firstBook.authors,
                        Publisher = firstBook.publisher,
                        PublishedDate = DateTime.Parse(firstBook.publishedDate),
                        MainDescription = firstBook.description,
                        TextSnippet = firstItem.searchInfo.textSnippet,
                        PageCount = firstBook.pageCount,
                        ThumbnailLink = firstBook.imageLinks.thumbnail
                    };

                    return book;
                }
            }
        }
    }
}
