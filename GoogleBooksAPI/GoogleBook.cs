﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleBooksAPI {
    public class GoogleBook {
        public string Title { get; set; }
        public List<string> Authors { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishedDate { get; set; }
        public string MainDescription { get; set; }
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public int PageCount { get; set; }
        public string ThumbnailLink { get; set; }
        public string TextSnippet { get; set; }
    }
}
