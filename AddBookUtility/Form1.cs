﻿using DataModels;
using GoogleBooksAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LinqToDB;

namespace AddBookUtility {
    public partial class Form1 : Form {

        public Form1() {
            InitializeComponent();


        }
        
        private async void searchButton_Click(object sender, EventArgs e) {
            UseWaitCursor = true;
            queryPanel.Enabled = false;

            currentTaskLabel.Text = "Searching for book...";

            var book = await search(queryBox.Text);
            if (book == null) {
                MessageBox.Show("Book not found...");
            } else {
                propertyGrid1.SelectedObject = book;
                await uploadBook(book);
            }

            currentTaskLabel.Text = "Awaiting input.";
            UseWaitCursor = false;
            queryPanel.Enabled = true;

            queryBox.Text = "";
            queryBox.Focus();
        }

        private async Task uploadBook(GoogleBook book) {
            currentTaskLabel.Text = "Uploading book...";

            using (var db = new CollectionDB()) {
                db.BeginTransaction();
                if (await db.Books.Where(b => b.ISBN10 == book.ISBN10 || b.ISBN13 == book.ISBN13).CountAsync() != 0) {
                    MessageBox.Show("Book already exists in DB.");
                } else {
                    Publisher publisher = await db.Publishers.FirstOrDefaultAsync(p => p.Name == book.Publisher);
                    if (publisher == null) {
                        publisher = new Publisher() { Name = book.Publisher };
                        publisher.ID = Convert.ToInt32(db.InsertWithIdentity(publisher));
                    }


                    Book b = new Book() {
                        ISBN10 = book.ISBN10,
                        ISBN13 = book.ISBN13,
                        NumberOfPages = book.PageCount,
                        PublishedDate = book.PublishedDate,
                        Title = book.Title,
                        Subtitle = book.Subtitle,
                        PublisherID = publisher.ID
                    };

                    b.ID = Convert.ToInt32(db.InsertWithIdentity(b));

                    Description d = new Description() { Description_Column = book.MainDescription, IsPrimary = 1, IsSnippet = 0, BookID = b.ID };
                    d.ID = Convert.ToInt32(db.InsertWithIdentity(d));
                    d = new Description() { Description_Column = book.TextSnippet, IsPrimary = 0, IsSnippet = 1, BookID = b.ID };
                    d.ID = Convert.ToInt32(db.InsertWithIdentity(d));
                    int index = 0;
                    foreach (var authorName in book.Authors) {
                        Author author = await db.Authors.FirstOrDefaultAsync(a => a.Name == authorName);
                        if (author == null) {
                            author = new Author() { Name = authorName };
                            author.ID = Convert.ToInt32(db.InsertWithIdentity(author));
                        }

                        db.Insert(new BookAuthor() { BookID = b.ID, AuthorID = author.ID, Index = index });
                        index++;
                    }

                    foreach (var catergoryString in book.Categories) {
                        Category cat = await db.Categories.FirstOrDefaultAsync(c => c.Name == catergoryString);
                        if (cat == null) {
                            cat = new Category() { Name = catergoryString };
                            cat.ID = Convert.ToInt32(db.InsertWithIdentity(cat));
                        }

                        db.Insert(new BookCategory() { BookID = b.ID, CategoryID = cat.ID });
                    }
                }
                db.CommitTransaction();
            }
        }

        private async Task<GoogleBook> search(string text) {
            return await Books.GetBookDetails(text);
        }

        private void Form1_Load(object sender, EventArgs e) {
            queryBox.Focus();
        }

        private void Form1_Enter(object sender, EventArgs e) {
            queryBox.Focus();
        }
    }
}
